#################################
#
#   Configuration stage
#
#################################
FROM jinja:latest as stage-configuration

LABEL maintainer="iugin"

ARG BIND_LOG_LEVEL="INFO"
# Path to the configuration directory
ARG BIND_CONFIG_DIR="/etc/bind"
ARG BIND_CONFIG_LOCAL_FILE
ARG BIND_CACHE_DIR="/var/cache/bind"
# Blacklist
ARG BIND_CONFIG_BLACKLIST_FILE
ARG BIND_BLACKLIST_SINKHOLE="127.0.0.1"
ARG BIND_BLACKLIST_SINKHOLE_V6="::1"
# acl list
ARG BIND_ACL
# Forwarders
ARG BIND_FORWARDERS
# Recursion
ARG BIND_RECURSION="no"
ARG BIND_RECURSION_ALLOW_ACL
# Allow only queries from
ARG BIND_ALLOW_ACL
# Listening on
ARG BIND_LISTEN_ON
ARG BIND_LISTEN_ON_V6
# Other
ARG BIND_DNSSEC_VALIDATION
ARG BIND_ALLOW_TRANSFER

COPY config /app/templates
RUN mkdir -p /app/config

# Use the engine that renders the /app/templates into the /app/config
RUN /app/entrypoint.sh jinja -i /app/templates -o /app/config -c -l $BIND_LOG_LEVEL

#################################
#
#   Final stage
#
#################################
FROM debian:buster-slim

LABEL maintainer="iugin"

ARG BIND_CONFIG_DIR="/etc/bind"
ENV BIND_CONFIG_DIR=${BIND_CONFIG_DIR}

# Clean output only during build
ARG DEBIAN_FRONTEND=noninteractive

# Install bind9
RUN apt-get update && apt-get install --no-install-recommends -y \
    bind9 \
    # setcap library:
    libcap2-bin

# Grant bind permission to privileged port
RUN setcap CAP_NET_BIND_SERVICE=+eip /usr/sbin/named

# Clean image
RUN set -e -x && \
    apt-get purge -y --auto-remove libcap2-bin && \
    rm -rf \
        /tmp/* \
        /var/tmp/* \
        /var/lib/apt/lists/*

# Copy the configuration
RUN rm -rf ${BIND_CONFIG_DIR} && mkdir -p ${BIND_CONFIG_DIR}
COPY --from=stage-configuration --chown=root:bind /app/config ${BIND_CONFIG_DIR}

# Copy the entrypoint
COPY --chown=root:bind entrypoint.sh /app/entrypoint.sh

WORKDIR /app

ENTRYPOINT [ "/app/entrypoint.sh" ]

USER bind:101

CMD [ "bind", "-g" ]