# Docker Bind9

This is a docker image to deploy and run a Bind9 server.
It is configurable using environment variables thanks to [Jinja](https://gitlab.com/neverenough/docker-images/jinja) and can be deployed with almost zero-effort.

## Security
This image was created with security and hardening as the main objective and the hope is to keep it always secure with continuous updates.

## Getting Started

The image is ready to be buildt and deployed using docker.

We provide an example of configuration and a possible `docker-compose` file that could be used to configure your own image.

### Prerequisities

In order to run this container you'll need docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

### Usage

#### Build

The only required parameter is the `BIND_FORWARDERS`.

To build a server with the default configuration, simply build the image specifying the `BIND_FORWARDERS`:

```shell
$ docker build --build-arg BIND_FORWARDERS=8.8.8.8 . -t bind9:latest
```

#### Run

To run the image with the default options:

```shell
$ docker run -p 53:53 bind9:latest
```

If you want to launch Bind9 with some custom options (advanced), you can override the default ones by concatenating the `bind` keyword followed by the custom options

```shell
$ docker run bind9:latest bind -g
```

However, if you want to execute arbitrary command inside the container (testing/debugging), you can concatenate such instruction to the launch command

```shell
$ docker run -it bind9:latest /bin/bash echo "Hello world!"
```

#### Build Arguments

Logging:
* [ *optional* ] `BIND_LOG_LEVEL` - the logging level. **Default**: *INFO*;

Configuration:
* [ *optional* ] `BIND_CONFIG_DIR` - the path to the configuration directory. **Default**: `/etc/bind`;
* [ *optional* ] `BIND_CACHE_DIR` - the cache directory. **Default**: `/var/cache/bind`;
* [ *optional* ] `BIND_CONFIG_LOCAL_FILE` - the path to the local configuration file. Any customization relative to the local zones must be specified from that file. It may be in any location and it may be left undefined;

Blacklist:
* [ *required* ] `BIND_FORWARDERS` - it binds the forwarders to which redirect the incoming unresolved requests;
* [ *optional* ] `BIND_CONFIG_BLACKLIST_FILE` - the path to the blacklist configuration file. It is in the format of any named configuration files. It may be left undefined;
* [ *optional* ] `BIND_BLACKLIST_SINKHOLE` - the sinkhole to wich all the blacklisted zones should redirect their resolutions. It may be a dummy address (e.g. 127.0.0.1) as well as it may redirect to a networked sinkhole that logs statistics regarding the resolutions of such blacklisted addresses. **Default**: `127.0.0.1`;
* [ *optional* ] `BIND_BLACKLIST_SINKHOLE_V6` - same as `BIND_BLACKLIST_SINKHOLE` but for IPv6. **Default**: ` -:1`;

Hardening:
* [ *optional* ] `BIND_ACL` - the list of possible acl to be defined. **WIP**: up until this point every acl maps *localnets*. It may be left undefined, in this case it cannot be used any restriction based on the acl in the following configurations;
* [ *optional* ] `BIND_RECURSION` - if set to *yes*, the dns server may perform recursion to resolve the queries. It impacts on the performance of the server and must be used with discretion. **Default**: *no*;
* [ *optional* ] `BIND_RECURSION_ALLOW_ACL` - if the recursion is enabled, than it can be used to restrict the availability of such function only to certain devices. Any acl defined here has to be defined in `BIND_ACL`. It can be left undefined, the server will perform recursion on requests generated from any client;
* [ *optional* ] `BIND_ALLOW_ACL` - restiction on the addresses allowed to perform any query on the server. Any acl defined here has to be defined in `BIND_ACL`. It can be left undefined, meaning no restriction will be applied;
* [ *optional* ] `BIND_LISTEN_ON` - it restricts the interfaces on which the server listens for requests. It can be left undefined, meaning that the server will listen on any interface;
* [ *optional* ] `BIND_LISTEN_ON_V6` - the same of `BIND_LISTEN_ON`, but for IPv6;
* [ *optional* ] `BIND_DNSSEC_VALIDATION` - whether or not to validate the dnssec certificates using the internal certificate store (e.g. *auto*). It can be left undefined;
* [ *optional* ] `BIND_ALLOW_TRANSFER` - if the dns server should enable zone transfering to other servers (e.g. *none*). It can be left undefined.

#### Environment Variables

* `BIND_CONFIG_DIR` - **WARNING**: do not ever change or override this variable! It must always be the same as the argument variable used during the build.

#### Volumes

In the docker-compose can be found some example of volumes that it is recomended to mount:
* the local configuration (`named.conf.local`);
* the blacklist configuration (`named.conf.blacklist`).

#### Useful File Locations

Some useful files can be found in:
* `config` - **WARNING**: do not ever change them! Those files are loaded during the build of the image and are automatically rendered using Jinja;
* `examples` - some useful examples. It is a starting point to create your own hardened dns server.

## Find Us

* [GitHub](https://gitlab.com/neverenough/docker-images/bind9)

## Authors

* **Eugenio Pierfederici** - *Initial work* - [GitHub](https://github.com/iuginP) [Gitlab](https://gitlab.com/iugin)
* **Eugenio Cavina** - *Contributions* - [Gitlab](https://gitlab.com/ecavina)
