#!/bin/bash

if [ "$1" = 'bind' ]; then
    shift
    # Check config
    /usr/sbin/named-checkconf -z "${BIND_CONFIG_DIR}/named.conf"
    if [ $? -ne 0 ]; then
        exit 1
    fi

    # Configure named execution
    set -- /usr/sbin/named -u bind -c "${BIND_CONFIG_DIR}/named.conf" "$@"
    if [ $? -ne 0 ]; then
        exit 1
    fi
fi

exec "$@"